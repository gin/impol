import { Component, Input, OnInit } from '@angular/core';
import { RestService } from './services/rest.service';
import { ResponseModel } from './models/response.model';
import { CalculateService } from './services/calculate.service';
import { RequestModel } from './models/request.model';
import { CoordinatesModel } from './models/coordinates.model';
import {map, catchError} from 'rxjs/internal/operators';
import { of } from 'rxjs';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  responseModel: ResponseModel;
  coordinates: CoordinatesModel;
  r: number;
  w: number;
  h: number;
  d: number;
  p: number;
  displayedColumns: string[] = ['position', 'x', 'y'];
  dataSource: CoordinatesModel[];

  constructor(
    private rest: RestService,
    private calcService: CalculateService,
  ) {}

  ngOnInit(): void {
    
  }

  async onSubmitTemplateBased() {
    //this.calcService.recalcualteForViewing(this.responseModel, this.r);
    console.log( 'Error call!' ,this.r);

    if(this.r == undefined || this.w == undefined || this.h == undefined || this.p == undefined || this.d == undefined ){
      console.log( 'Not all parameter!');
      return of();
    }

    const formData = new FormData();
    formData.append('polmer', this.r.toString() );
    formData.append('dolzina', this.w.toString() );
    formData.append('sirina', this.h.toString() );
    formData.append('rob', this.p.toString() );
    formData.append('minRaz', this.d.toString() );

    this.rest.calculate(formData).pipe(
      catchError(
        (err) => {
          console.log( 'Error call!');
          return of(); //vrne observebel catch metoda
        }
      )
    ).subscribe(
      (res) => {
        this.responseModel = res;
        this.dataSource = res.coordinates;
        this.calcService.recalculateViewWindow(this.responseModel, this.w, this.h, this.r);
      }
    );
  }
}
