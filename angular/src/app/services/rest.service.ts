import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseModel } from '../models/response.model';
import { RequestModel } from '../models/request.model';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(
    private http: HttpClient,
  ) {}

  calculate(formData: FormData) {
    return this.http.post<any>('http://localhost:51467/api/Plosca', formData); //vrne rest call samose se ne pozove pozove se ko dam subscribe
  }
  
}