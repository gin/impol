export class CoordinatesModel {
  id: number;
  x: number;
  y: number;
  r: number;
  s: number;
}
