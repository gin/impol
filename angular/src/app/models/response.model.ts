import { CoordinatesModel } from './coordinates.model';

export class ResponseModel {
  sirina: number;
  dolzina: number;
  rob: number;
  polmer: number;
  minRaz: number;
  maxRondelic: number;
  coordinates: CoordinatesModel[];
}
