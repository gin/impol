﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using rest2.Controllers.Models;
using rest2.Models;
using rest2.Services;
using System;
using System.Net.Http;

namespace rest2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PloscaController : ControllerBase
    {
        private readonly IDataService _dataService;

        private readonly ICircleService _circleService;

        public PloscaController(ICircleService circleService)
        {
            _circleService = circleService;
        }

        // GET: api/Plosca
        [HttpGet]
        public IActionResult Get()
        {
            return null;
        }

        // GET: api/Plosca/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Plosca
        [HttpPost]
        public IActionResult Post([FromForm] CirclePackingRequest request)
        {
            var records = _circleService.Calculate(request);
           // var records = _dataService.Get();
            if (records != null)
                return Ok(records);
            return NotFound();
        }

        // PUT: api/Plosca/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
