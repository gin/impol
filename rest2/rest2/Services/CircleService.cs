﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CirclePackingLib;
using rest2.Controllers.Models;
using rest2.Models;

namespace rest2.Services
{
    public class CircleService : ICircleService
    {
        private readonly ICalculateRondelica _calculateRondelica;

        public CircleService(ICalculateRondelica calculateRondelica)
        {
            _calculateRondelica = calculateRondelica;
        }

        public Plosca Calculate(CirclePackingRequest cpr)
        {
            
            double novRobe = cpr.rob;
            double newPolmer = cpr.polmer;
            if (cpr.minRaz > 0)
            {
                newPolmer = _calculateRondelica.NewRadiusSum(cpr.polmer, cpr.minRaz / 2);
                if (cpr.rob > (cpr.minRaz / 2))
                {
                    novRobe = cpr.minRaz / 2;
                }
            }

            double newDolzina = _calculateRondelica.NewMeasureDiv(cpr.dolzina, novRobe);
            double newSirina = _calculateRondelica.NewMeasureDiv(cpr.sirina, novRobe);

            double visina = _calculateRondelica.Visina(newPolmer);
            int krogiNaDolzina = _calculateRondelica.KrogiNaStranico(newDolzina, newPolmer);
            int krogiNaVisino = _calculateRondelica.KrogiNaStranico(newSirina, newPolmer);

            int krogov = _calculateRondelica.CircleM(newDolzina, newPolmer);
            int vrstic = _calculateRondelica.CircleN(newSirina, visina, newPolmer);
            int maxRondelic = _calculateRondelica.MulKrogov(krogov, vrstic);

            List<Rondelica> lstRondelic = new List<Rondelica>();

            lstRondelic = Cordinats(krogov, vrstic, newPolmer, visina, cpr.minRaz, novRobe, newDolzina);

            

            Plosca plosca = new Plosca();

            plosca.dolzina = cpr.dolzina;
            plosca.sirina = cpr.sirina;
            plosca.polmer = cpr.polmer;
            plosca.rob = novRobe*2;
            plosca.minRaz = cpr.minRaz;
            plosca.maxRondelic = lstRondelic.Count;
            plosca.coordinates = lstRondelic;


            return plosca;
        }

        private List<Rondelica> Cordinats(int krogov, int vrstic, double polmer, double visina, double minRaz, double rob, double dolzina)
        {

            List<Rondelica> lstRondelic = new List<Rondelica>();
            int count = 1;
            double yy = polmer;
            double xx = 0.0;
            int krogovO = krogov;
            for (double i = 1; i <= vrstic; i ++ ) {
                if (i == 1)
                {
                    yy = rob + polmer;
                } else
                {
                    yy = yy + visina;
                }

                if (i % 2 == 0)
                {
                    xx = rob + (polmer * 2);
                    krogov = krogovO - 1;
                }
                else
                {
                    xx = rob + polmer;
                    krogov = krogovO;
                }
                for (double j = 1 ; j <= krogov; j++)
                {
                   
                    Rondelica rondelica = new Rondelica();
                    rondelica.id = count;
                    rondelica.x = xx;
                    rondelica.y = yy;
                    rondelica.r = polmer;
                    rondelica.s = minRaz/2;
                    lstRondelic.Add(rondelica);
                    xx = xx + (polmer * 2);
                    count++;
                    if(j == krogov ) { 
                        if( xx + polmer <= dolzina || (xx <= dolzina && rob>0)) {
                            Rondelica rondelicaL = new Rondelica();
                            rondelica.id = count;
                            rondelicaL.x = xx;
                            rondelicaL.y = yy;
                            rondelicaL.r = polmer;
                            rondelicaL.s = minRaz / 2;
                            lstRondelic.Add(rondelicaL);
                            count++;
                        }
                    }
                }

                
            }

            return lstRondelic;
        }
    }
}
