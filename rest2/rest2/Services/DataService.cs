﻿using rest2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rest2.Services
{
    public class DataService : IDataService
    {
        private readonly List<Rondelica> lstRondelic = new List<Rondelica>();
        private readonly Plosca plosca = new Plosca();

        public DataService()
        {

        }

        public Plosca Get()
        {
            if (plosca != null )
                return plosca;
            return null;
        }
    }
}
