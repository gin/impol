﻿using rest2.Controllers.Models;
using rest2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rest2.Services
{
    public interface ICircleService
    {
        Plosca Calculate(CirclePackingRequest cpr);
    }
}
