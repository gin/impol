﻿using rest2.Models;
using System.Collections.Generic;

namespace rest2.Services
{
    public interface IDataService
    {
        Plosca Get();
    }
}
