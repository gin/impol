﻿namespace rest2.Controllers.Models
{
    public class CirclePackingRequest
    {
        public double polmer { get; set; }
        public double dolzina { get; set; }
        public double sirina { get; set; }
        public double rob { get; set; }
        public double minRaz { get; set; }

    }
}