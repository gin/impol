﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rest2.Models
{
    public class Rondelica
    {
        public int id { get; set; }
        public double x { get; set; }
        public double y { get; set; }
        public double r { get; set; }
        public double s { get; set; }
    }
}
