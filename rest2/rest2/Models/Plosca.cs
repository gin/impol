﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rest2.Models
{
    public class Plosca
    {
        public double dolzina { get; set; }
        public double sirina { get; set; }
        public double polmer { get; set; }
        public double minRaz { get; set; }
        public double rob { get; set; }
        public int maxRondelic { get; set; }

        public List<Rondelica> coordinates { get; set; }
    }
}
