﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CirclePackingLib
{
    public class CalculateRondelica : ICalculateRondelica
    {

        public int CircleM(double dolzina, double polmer)
        {
            double m = (dolzina / polmer) / 2;
            return (int)Math.Floor(m);

        }

        public int CircleN(double sirina, double visina, double polmer)
        {
            //double n = ((sirina - (2 * polmer)) / visina) + 1;
            double n = (((sirina / polmer) - 2) / Math.Sqrt(3)) + 1;
            return (int)Math.Floor(n);

        }

        public double NewRadiusSum(double original, double sum)
        {
            return original + sum;
        }

        public double NewMeasureDiv(double original, double div)
        {
            return original - div*2;
        }

        public int KrogiNaStranico(double dolzina, double polmer)
        {
            return (int)Math.Floor(dolzina / (2 * polmer));
        }

        public double Visina(double polmer)
        {
            return (Math.Sqrt(3) / 2) * (2 * polmer);
        }

        public int MulKrogov(int m, int n)
        {
            return m * n;
        }

    }
}
