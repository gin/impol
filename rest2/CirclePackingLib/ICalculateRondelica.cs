﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CirclePackingLib
{
    public interface ICalculateRondelica
    {
        int CircleM(double sirina, double polmer);

        int CircleN(double dolzina, double visina, double polmer);

        double NewRadiusSum(double original, double sum);

        double NewMeasureDiv(double original, double div);

        int KrogiNaStranico(double dolzina, double polmer);

        double Visina(double polmer);

        int MulKrogov(int m, int n);

    }
}
