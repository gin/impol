using CirclePackingLib;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void CalculatM()
        {
            CalculateRondelica cr = new CalculateRondelica();
            Assert.AreEqual(6, cr.CircleM(13,1));
        }

        [Test]
        public void CalculatN()
        {
            CalculateRondelica cr = new CalculateRondelica();
            Assert.AreEqual(7,cr.CircleN(13,1.733,1));
        }
    }
}